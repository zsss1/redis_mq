package com.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.listener.Topic;
import org.springframework.stereotype.Component;

import com.example.listener.MyMessageListener;
import com.example.listener.TopicChannel;

@Component
public class RedisConfig {
    @Bean
    public MessageListenerAdapter messageListenerAdapter() {
        return new MessageListenerAdapter(new MyMessageListener());
    }

    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory, MessageListenerAdapter messageListenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        // messageListenerAdapter 订阅 SEND_EMAIL 频道
        container.addMessageListener(messageListenerAdapter, new PatternTopic(TopicChannel.SEND_EMAIL));
        // messageListenerAdapter 订阅 SEND_PHONE 频道
        container.addMessageListener(messageListenerAdapter, new PatternTopic(TopicChannel.SEND_PHONE));
        return container;
    }
}
