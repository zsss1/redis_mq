package com.example.listener;

import org.junit.jupiter.api.Test;
import org.redisson.api.listener.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import com.example.demo.DemoApplication;

@SpringBootTest(classes = DemoApplication.class)
public class MyListener {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Test
    public void test_pub() {
        redisTemplate.convertAndSend(TopicChannel.SEND_EMAIL, "pub email message");
        redisTemplate.convertAndSend(TopicChannel.SEND_PHONE, "pub phone message");
    }
}
